import 'dart:convert';

/// Represent a contact
class Contact {
  String _name;
  String _phoneNumber;

  /// displayed name
  String get name => _name;
  /// phone number
  String get phoneNumber => _phoneNumber;

  /// Create Contact object from map
  Contact.fromMap(Map<String, dynamic> map) {
    _name = map['display_name'];
    _phoneNumber = map['phone_number'];
  }

  /// Returns a string representation of this message.
  /// The returned string is a JSON like indented string
  @override
  String toString() => const JsonEncoder.withIndent('  ')
    .convert({
      'name': _name,
      'phoneNumber': _phoneNumber
    });
}