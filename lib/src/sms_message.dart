import 'dart:convert';

///S ms message type
enum SmsMessageType {
  /// 0
  ALL,
  /// 1
  INBOX,
  /// 2
  SENT,
  /// 3
  DRAFT,
  /// 4
  OUTBOX,
  /// 5
  FAILED
}

/// Sms message status
enum SmsMessageStatus {
  /// -1
  NONE,
  /// 0
  COMPLETE,
  /// 32
  PENDING,
  /// 64
  FAILED
}

///A SMS message
class SmsMessage implements Comparable<SmsMessage> {
    int _id;
    String _address;
    String _body;
    String _creator;
    DateTime _date;
    DateTime _dateSent;
    int _errorCode;
    bool _locked;
    int _person;
    int _protocol;
    bool _read;
    bool _replyPathPresent;
    bool _seen;
    String _serviceCenter;
    SmsMessageStatus _status;
    String _subject;
    int _threadId;
    SmsMessageType _type;

  /// Create a SMS message from a mapped message
  SmsMessage.fromMap(Map<String, dynamic> map) {
    _id = map['_id'];
    _address = map['address'];
    _body = map['body'];
    _creator = map['creator'];
    _date = DateTime.fromMillisecondsSinceEpoch(map['date']);
    _dateSent = DateTime.fromMillisecondsSinceEpoch(map['date_sent']);
    _errorCode = map['error_code'];
    _locked = map['locked'] != 0;
    _person = map['person'];
    _protocol = map['protocol'];
    _read = map['read'] != 0;
    _replyPathPresent = map['replay_path_present'] != 0;
    _seen = map['seen'] != 0;
    _serviceCenter = map['service_center'];
    _status = SmsMessageStatus.values[(map['status'] + 1) % 31];
    _subject = map['subject'];
    _threadId = map['thread_id'];
    _type = SmsMessageType.values[map['type']];
  }

    ///The Message ID
    int get id => _id;
    ///The address of the other party.
    String get address => _address;
    ///The body of the message.
    String get body => _body;
    ///The identity of the sender of a sent message.
    String get creator => _creator;
    ///The date the message was received.
    DateTime get date => _date;
    ///The date the message was sent.
    DateTime get dateSent => _dateSent;
    ///Error code associated with sending or receiving message
    int get errorCode => _errorCode;
    ///Is the message locked?
    bool get locked => _locked;
    ///The ID of the sender of the conversation, if present. (contacts)
    int get person => _person;
    ///The protocol identifier code.
    int get protocol => _protocol;
    ///Has the message been read?
    bool get read => _read;
    ///Is the "TP-Reply-Path" flag set?
    bool get replyPathPresent => _replyPathPresent;
    ///Has the message been seen by the user? The "seen" flag determines whether
    ///we need to show a notification.
    bool get seen => _seen;
    ///The service center (SC) through which to send the message, if present.
    String get serviceCenter => _serviceCenter; // Phone number +XX...
    ///TP-Status value for the message, or -1 if no status has been received.
    SmsMessageStatus get status => _status;
    ///The subject of the message, if present.
    String get subject => _subject;
    ///The thread ID of the message.
    int get threadId => _threadId;
    ///Message type
    SmsMessageType get type => _type;

  @override
  int compareTo(SmsMessage other) {
    return other._id - _id;
  }

  /// Returns a string representation of this message.
  /// The returned string is a JSON like indented string
  @override
  String toString() {
    final JsonEncoder encoder = const JsonEncoder.withIndent('  ');
    return encoder.convert({
      'id': _id,
      'address': _address,
      'body': _body,
      'creator': _creator,
      'date': _date.millisecondsSinceEpoch,
      'dateSent': _dateSent.millisecondsSinceEpoch,
      'errorCode': _errorCode,
      'locked': _locked,
      'person': _person,
      'protocol': protocol,
      'read': _read,
      'replyPathPresent': _replyPathPresent,
      'seen': _seen,
      'serviceCenter': _serviceCenter,
      'status': _status.toString(),
      'subject': _subject,
      'threadId': _threadId,
      'type': _type.toString(),
    });
  }
}