// A SMS Plugin for flutter
library sms_services;

import 'dart:async';

import 'package:flutter/services.dart';
import 'src/contact.dart';
import 'src/sms_message.dart';

export 'src/contact.dart';
export 'src/sms_message.dart';

/// SMS Services entry point class
class SmsServices {
  static final MethodChannel _channel = const MethodChannel('sms_services', JSONMethodCodec());
  static final EventChannel _eventChannel = const EventChannel('sms_services/stream', JSONMethodCodec());

  /// Get All Sms sorted by later date
  /// Can pass selector arguments as Map
  /// Return null if an exception is catched (Permission denied)
  static Future<List<SmsMessage>> getSms({
    int id,
    String address,
    String body,
    String creator,
    DateTime date,
    DateTime dateSent,
    int errorCode,
    bool locked,
    int person,
    int protocol,
    bool read,
    bool replyPathPresent,
    bool seen,
    String serviceCenter,
    SmsMessageStatus status,
    String subject,
    int threadId,
    SmsMessageType type
  }) async {
    final args = {};
    if (id != null) {
      args['_id'] = id;
    }
    if (address != null) {
      args['address'] = address;
    }
    if (body != null) {
      args['body'] = body;
    }
    if (creator != null) {
      args['creator'] = creator;
    }
    if (date != null) {
      args['date'] = date.millisecondsSinceEpoch;
    }
    if (dateSent != null) {
      args['date_sent'] = dateSent.millisecondsSinceEpoch;
    }
    if (errorCode != null) {
      args['error_code'] = errorCode;
    }
    if (locked != null) {
      args['locked'] = locked ? 1 : 0;
    }
    if (person != null) {
      args['person'] = person;
    }
    if (protocol != null) {
      args['protocol'] = protocol;
    }
    if (read != null) {
      args['read'] = read ? 1 : 0;
    }
    if (replyPathPresent != null) {
      args['reply_path_present'] = replyPathPresent ? 1 : 0;
    }
    if (seen != null) {
      args['seen'] = seen ? 1 : 0;
    }
    if (serviceCenter != null) {
      args['service_center'] = serviceCenter;
    }
    if (status != null) {
      args['status'] = status.index != 0 ? 32 * status.index : -1;
    }
    if (subject != null) {
      args['subject'] = subject;
    }
    if (threadId != null) {
      args['thread_id'] = threadId;
    }
    if (type != null) {
      args['type'] = type.index;
    }

    var result;
    try {
      result = List<SmsMessage>.from((
          await _channel.invokeListMethod<Map<String, dynamic>>('getSms', args))
            .map((map) => SmsMessage.fromMap(map)));
    } on PlatformException catch(e) {
      print(e);
    }
    return result;
  }

  /// Get the first line phone number
  static Future<String> getPhoneNum({bool localFormat = true}) =>
    _channel.invokeMethod('getPhoneNum', localFormat);

  /// Get the list of contact names
  static Future<List<Contact>> getContacts() async =>
    List<Contact>.of((
        await _channel.invokeListMethod<Map<String, dynamic>>('getContacts'))
            .map((map) => Contact.fromMap(map)));

  /// Send a SMS
  static Future<bool> sendSms({String destinationAddress, String message}) async {
    final args = {};
    args['destinationAddress'] = destinationAddress;
    args['message'] = message;
    return _channel.invokeMethod('sendSms', args);
  }

  /// Get a strean=m that receive incoming SMS
  static Stream<dynamic> receiveSms() {
    return _eventChannel.receiveBroadcastStream();
  }
}
