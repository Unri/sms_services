package com.example.sms_services

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.Manifest;
import android.provider.Telephony;
import android.provider.Telephony.TextBasedSmsColumns;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import io.flutter.plugin.common.EventChannel.StreamHandler;
import io.flutter.plugin.common.EventChannel.EventSink;

import org.json.JSONObject

class SmsReceiver: StreamHandler {
    var receiver: BroadcastReceiver? = null
    val activity: Activity;

    constructor(activity: Activity): super() {
        this.activity = activity;
    }

    override fun onListen(args: Any?, events: EventSink) {
        this.receiver = createReceiver(events);
        activity.registerReceiver(this.receiver, IntentFilter(Telephony.Sms.Intents.SMS_RECEIVED_ACTION));
    }

    override fun onCancel(args: Any?) {
        activity.unregisterReceiver(this.receiver);
    }

    private fun createReceiver(events: EventSink): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                try {
                    val msgs = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                    if (msgs == null)
                        return;
                    val obj = JSONObject();
                    obj.put(TextBasedSmsColumns.ADDRESS, msgs[0].getOriginatingAddress());
                    obj.put(TextBasedSmsColumns.BODY, msgs.joinToString("") { it -> it.getMessageBody() });
                    obj.put(TextBasedSmsColumns.CREATOR, BuildConfig.APPLICATION_ID);
                    obj.put(TextBasedSmsColumns.DATE, System.currentTimeMillis());
                    obj.put(TextBasedSmsColumns.DATE_SENT, msgs[0].getTimestampMillis());
                    obj.put(TextBasedSmsColumns.ERROR_CODE, 0);
                    obj.put(TextBasedSmsColumns.LOCKED, 0);
                    obj.put(TextBasedSmsColumns.PERSON, 0)
                    obj.put(TextBasedSmsColumns.PROTOCOL, 0);
                    obj.put(TextBasedSmsColumns.READ, if (msgs[0].getStatusOnIcc() == SmsManager.STATUS_ON_ICC_READ) 1 else 0);
                    obj.put(TextBasedSmsColumns.REPLY_PATH_PRESENT, 0);
                    obj.put(TextBasedSmsColumns.SEEN, 1);
                    obj.put(TextBasedSmsColumns.STATUS, -1);
                    obj.put(TextBasedSmsColumns.THREAD_ID, getThreadIdFromAddress(msgs[0].getOriginatingAddress()));
                    obj.put(TextBasedSmsColumns.TYPE, Telephony.Sms.MESSAGE_TYPE_INBOX);
                    events.success(obj);
                } catch (e: Exception) {
                    Log.e("SmsReceiver", e.toString());
                }
            }
        }
    }

    private fun getThreadIdFromAddress(address: String): Int {
        val contentResolver: ContentResolver = activity.getContentResolver();
        val cursor: Cursor? = contentResolver.query(
            Telephony.Sms.CONTENT_URI,
            arrayOf(TextBasedSmsColumns.THREAD_ID),
            TextBasedSmsColumns.ADDRESS + " = ?",
            arrayOf(address),
            null
        );
        if (cursor == null)
            return 0;
        cursor.moveToFirst();
        return cursor.getInt(cursor.getColumnIndex(TextBasedSmsColumns.THREAD_ID));
    }
}
