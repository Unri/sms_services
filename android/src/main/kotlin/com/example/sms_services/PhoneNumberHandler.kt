import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.PhoneNumberUtils;

class PhoneNumberHandler {
    companion object {
        fun getPhoneNumber(context: Context, localFormat: Boolean = true): String {
            val manager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager;
            val formatter = if (localFormat) PhoneNumberUtils::formatNumber
                else PhoneNumberUtils::formatNumberToE164;
            return formatter(
                    manager.getLine1Number(),
                    manager.getNetworkCountryIso().toUpperCase()
                );
        }
    }
}