package com.example.sms_services

import android.app.Activity;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.Manifest;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.util.Log;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;

import org.json.JSONObject

class SmsSendHandler
(
    val activity: Activity,
    val args: JSONObject,
    val result: MethodChannel.Result
    ): RequestPermissionsResultListener {

    companion object {
        const val permission = Manifest.permission.SEND_SMS;
        const val SEND_SMS_REQUEST_CODE = 3;
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
        ): Boolean {
            if (requestCode != SEND_SMS_REQUEST_CODE) {
                return false;
            }
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                result.success(sendSms(args));
            } else {
                result.error("error", permission + " denied", "on sendSms");
            }
            return true;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    fun sendSms(args: JSONObject): Boolean {
        val destinationAddress = args.getString("destinationAddress");
        val messageBody = args.getString("message");

        val smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(destinationAddress, null, messageBody, null, null);
        return true;
    }
}