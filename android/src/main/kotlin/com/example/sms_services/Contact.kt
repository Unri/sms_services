package com.example.sms_services

import android.database.Cursor
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import java.util.Locale;
import org.json.JSONObject
/**
 *  Represent an SMS
 */
data class Contact(
    /**
     * Displayed name
     */
    val display_name: String,
    /**
     * Phone number
     */
    val phone_number: String
) {
    companion object {
        private val colNames = mapOf(
            ContactsContract.Contacts.DISPLAY_NAME to "display_name",
            ContactsContract.CommonDataKinds.Phone.NUMBER to "phone_number"
        );

        /**
         * Get a SmsMessage as JSONObject from Sms record pointed by a cursor
         * @param cursor The cursor pointing on a Sms record
         * @return JSONObject The converted Sms
         */
        fun getJSONObject(cursor: Cursor): JSONObject {
            val obj: JSONObject = JSONObject();
            for (i in 0 until cursor.columnCount) {
                val columnName: String = cursor.getColumnName(i);
                when (columnName) {
                    ContactsContract.Contacts.DISPLAY_NAME ->
                        obj.put(colNames[columnName], cursor.getString(i));
                    ContactsContract.CommonDataKinds.Phone.NUMBER ->
                        obj.put(colNames[columnName],
                            PhoneNumberUtils.formatNumberToE164(
                                cursor.getString(i),
                                Locale.getDefault().getCountry())
                            ?: cursor.getString(i));
                }
            }
            return obj;
        }
    }
}
