package com.example.sms_services

import android.app.Activity;
import android.content.pm.PackageManager;
import android.Manifest;
import android.os.Build;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;

class Permission(
    val activity: Activity,
    val permissionRegistry: (RequestPermissionsResultListener) -> Any
    ) {

    fun requestPermissions(
        requestCode: Int,
        permissions: Array<String>,
        handler: RequestPermissionsResultListener
    ) {
        if (permissions.all { hasPermission(it) }) {
            handler.onRequestPermissionsResult(
                requestCode,
                permissions,
                IntArray(permissions.size){ PackageManager.PERMISSION_GRANTED }
            );
        } else {
            permissionRegistry(handler);
            for (permission in permissions) {
                if (!hasPermission(permission)) {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(activity,
                            arrayOf(permission),
                            requestCode)
                }
            }
        }
    }

    private fun hasPermission(permission: String): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M
            || activity.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
    }
}
