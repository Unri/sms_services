package com.example.sms_services

import android.app.Activity;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.Manifest;
import android.os.Build;
import android.provider.Telephony;
import android.provider.Telephony.TextBasedSmsColumns;
import android.provider.BaseColumns;
import android.util.Log;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;

import org.json.JSONObject

class SmsQueryHandler(
    val activity: Activity,
    val args: JSONObject,
    val result: MethodChannel.Result
    ): RequestPermissionsResultListener {

    companion object {
        const val permission = Manifest.permission.READ_SMS;
        const val READ_SMS_REQUEST_CODE = 1;
        const val SORT_ORDER = "date ASC";
        val sms_fields = arrayOf<String>(
            BaseColumns._ID,
            TextBasedSmsColumns.ADDRESS,
            TextBasedSmsColumns.BODY,
            TextBasedSmsColumns.CREATOR,
            TextBasedSmsColumns.DATE,
            TextBasedSmsColumns.DATE_SENT,
            TextBasedSmsColumns.ERROR_CODE,
            TextBasedSmsColumns.LOCKED,
            TextBasedSmsColumns.PERSON,
            TextBasedSmsColumns.PROTOCOL,
            TextBasedSmsColumns.READ,
            TextBasedSmsColumns.REPLY_PATH_PRESENT,
            TextBasedSmsColumns.SEEN,
            TextBasedSmsColumns.SERVICE_CENTER,
            TextBasedSmsColumns.STATUS,
            TextBasedSmsColumns.THREAD_ID,
            TextBasedSmsColumns.TYPE
        );
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
        ): Boolean {
            if (requestCode != READ_SMS_REQUEST_CODE) {
                return false;
            }
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                result.success(getAllSms(args));
            } else {
                result.error("error", permission + " denied", "on getAllSms");
            }
            return true;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    fun getAllSms(args: JSONObject): List<JSONObject> {
        val list = mutableListOf<JSONObject>();
        val selection = mutableListOf<String>();
        val selection_args = mutableListOf<String>();
        for (key in args.keys()) {
            selection.add(key);
            selection_args.add(args.getString(key))
        }

        val contentResolver: ContentResolver = activity.getContentResolver();
        val cursor: Cursor? = contentResolver.query(
            Telephony.Sms.CONTENT_URI,
            sms_fields,
            selection.joinToString(" AND ") { it -> it + " = ?" },
            selection_args.toTypedArray(),
            SORT_ORDER
        );

        if (cursor == null) {
            list.add(JSONObject("""{"error":"Permission denied: SmsQuery"}"""));
        } else {
            while (cursor.moveToNext()) {
                list.add(SmsMessage.getJSONObject(cursor));
            }
            cursor.close();
        }
        return list;
    }
}