package com.example.sms_services

import android.database.Cursor
import android.telephony.PhoneNumberUtils;
import java.util.Locale;
import org.json.JSONObject

/**
 *  Represent an SMS
 */
data class SmsMessage(
    /**
     * The SMS ID
     */
    val _id: Int,
    /**
     * The address of the other party.
     */
    val address: String,
    /**
     * The body of the message.
     */
    val body: String,
    /**
     * The identity of the sender of a sent message.
     * like: com.google.android.apps.messaging
     */
    val creator: String,
    /**
     * The date the message was received.
     */
    val date: Long,
    /**
     * The date the message was sent.
     */
    val date_sent: Long,
    /**
     * Error code associated with sending or receiving this message
     */
    val error_code: Int,
    /**
     * Is the message locked?
     */
    val locked: Boolean,
    /**
     * The ID of the sender of the conversation, if present. (contacts)
     */
    val person: Int?,
    /**
     * The protocol identifier code.
     */
    val protocol: Int,
    /**
     * Has the message been read?
     */
    val read: Boolean,
    /**
     * Is the "TP-Reply-Path" flag set?
     */
    val reply_path_present: Boolean,
    /**
     * Has the message been seen by the user? The "seen" flag determines whether
     * we need to show a notification.
     */
    val seen: Boolean,
    /**
     * The service center (SC) through which to send the message, if present.
     */
    val service_center: String?, // Phone number +XX...
    /**
     * TP-Status value for the message, or -1 if no status has been received.
     */
    val status: Int = -1,
    /**
     * The subject of the message, if present.
     */
    val subject: String?,
    /**
     * The thread ID of the message.
     */
    val thread_id: Int,
    /**
     * Message type
     */
    val type: Int
) {
    companion object {
        /**
         * Get a SmsMessage as JSONObject from Sms record pointed by a cursor
         * @param cursor The cursor pointing on a Sms record
         * @return JSONObject The converted Sms
         */
        fun getJSONObject(cursor: Cursor): JSONObject {
            val obj: JSONObject = JSONObject();
            for (i in 0 until cursor.columnCount) {
                val columnName: String = cursor.getColumnName(i);
                when (columnName) {
                    "date",
                    "date_sent" -> obj.put(columnName, cursor.getLong(i));
                    "address" -> obj.put(columnName,
                        PhoneNumberUtils.formatNumberToE164(
                            cursor.getString(i),
                            Locale.getDefault().getCountry())
                        ?: cursor.getString(i));
                    "body",
                    "creator",
                    "service_center" -> obj.put(columnName, cursor.getString(i));
                    else -> obj.put(columnName, cursor.getInt(i));
                }
            }
            return obj;
        }
    }
}
