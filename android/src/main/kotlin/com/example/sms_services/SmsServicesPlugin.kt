package com.example.sms_services

import android.app.Activity;
import android.util.Log;
import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;

/** SmsServicesPlugin */
public class SmsServicesPlugin: FlutterPlugin, ActivityAware {

  var flutterPluginBinding: FlutterPlugin.FlutterPluginBinding? = null;
  var methodCallHandler: MethodCallHandlerImpl? = null;

  override fun onAttachedToEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
      this.flutterPluginBinding = binding;
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val plugin = SmsServicesPlugin();
      plugin.startListening(
        registrar.activity(),
        registrar.messenger(),
        registrar::addRequestPermissionsResultListener
      );
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    this.flutterPluginBinding = null;
  }

  override fun onAttachedToActivity(@NonNull binding: ActivityPluginBinding) {
      startListening(
        binding.getActivity(),
        flutterPluginBinding!!.getBinaryMessenger(),
        binding::addRequestPermissionsResultListener
      );
  }

  override fun onDetachedFromActivityForConfigChanges() {
    // destroyed to change configuration.
    // This call will be followed by onReattachedToActivityForConfigChanges().
    onDetachedFromActivity()
  }

  override fun onReattachedToActivityForConfigChanges(@NonNull binding: ActivityPluginBinding) {
    // after a configuration change.
    onAttachedToActivity(binding);
  }

  override fun onDetachedFromActivity() {
    // Clean up references.
    methodCallHandler?.stopListening();
    methodCallHandler = null;
  }

  private fun startListening(
    activity: Activity,
    messenger: BinaryMessenger,
    permissionRegistry: (RequestPermissionsResultListener) -> Any
  ) {
    methodCallHandler = MethodCallHandlerImpl(
      activity, messenger, Permission(activity, permissionRegistry));
  }
}
