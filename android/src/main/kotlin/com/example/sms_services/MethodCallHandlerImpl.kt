package com.example.sms_services

import android.app.Activity;
import android.Manifest;
import android.util.Log;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.JSONMethodCodec
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

import org.json.JSONObject

class MethodCallHandlerImpl : MethodChannel.MethodCallHandler {

    var methodChannel: MethodChannel;
    var eventChannel: EventChannel;
    val activity: Activity;
    val messenger: BinaryMessenger;
    val permission: Permission;

    constructor(
        activity: Activity,
        messenger: BinaryMessenger,
        permission: Permission
    ) {
        this.activity = activity;
        this.messenger = messenger;
        this.permission = permission;
        methodChannel = MethodChannel(messenger, "sms_services", JSONMethodCodec.INSTANCE);
        eventChannel = EventChannel(messenger, "sms_services/stream", JSONMethodCodec.INSTANCE);
        eventChannel.setStreamHandler(SmsReceiver(activity));
        methodChannel.setMethodCallHandler(this);
    }

    fun stopListening() {
        methodChannel?.setMethodCallHandler(null);
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when(call.method) {
            "getSms" -> permission.requestPermissions(
                SmsQueryHandler.READ_SMS_REQUEST_CODE,
                arrayOf(Manifest.permission.READ_SMS),
                SmsQueryHandler(activity, call.arguments(), result)
            );
            "getPhoneNum" -> result.success(PhoneNumberHandler.getPhoneNumber(
                activity, call.arguments as Boolean)
            );
            "getContacts" -> permission.requestPermissions(
                ContactQueryHandler.READ_CONTACTS_REQUEST_CODE,
                arrayOf(Manifest.permission.READ_CONTACTS),
                ContactQueryHandler(activity, result)
            );
            "sendSms" -> permission.requestPermissions(
                SmsSendHandler.SEND_SMS_REQUEST_CODE,
                arrayOf(Manifest.permission.SEND_SMS),
                SmsSendHandler(activity, call.arguments(), result)
            );
        }
    }
}