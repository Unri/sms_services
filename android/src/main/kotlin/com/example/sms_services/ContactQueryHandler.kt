package com.example.sms_services

import android.app.Activity;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.Manifest;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;

import org.json.JSONObject

class ContactQueryHandler(
    val activity: Activity,
    val result: MethodChannel.Result
    ): RequestPermissionsResultListener {

    companion object {
        const val permission = Manifest.permission.READ_CONTACTS;
        const val READ_CONTACTS_REQUEST_CODE = 2;
        val contact_fields = arrayOf<String>(
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
        );
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
        ): Boolean {
            if (requestCode != READ_CONTACTS_REQUEST_CODE) {
                return false;
            }
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                result.success(getAllContacts());
            } else {
                result.error("error", permission + " denied", "on getAllContacts");
            }
            return true;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    fun getAllContacts(): List<JSONObject> {
        val list = mutableListOf<JSONObject>();

        val contentResolver: ContentResolver = activity.getContentResolver();
        val cursor: Cursor?  = contentResolver.query(
            ContactsContract.Data.CONTENT_URI,
            contact_fields,
            "${ContactsContract.CommonDataKinds.Phone.TYPE} = ?",
            arrayOf(ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE.toString()),
            "${ContactsContract.Contacts.DISPLAY_NAME} ASC"
        );

        if (cursor == null) {
            list.add(JSONObject("""{"error":"Permission denied: ContactsQuery"}"""));
        } else {
            while(cursor.moveToNext()) {
                list.add(Contact.getJSONObject(cursor));
            }
            cursor.close();
        }
        return list;
    }
}