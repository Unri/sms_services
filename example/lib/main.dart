import 'dart:async';
import 'package:flutter/material.dart';

import 'package:sms_services/sms_services.dart';

void main() => runApp(MyApp());
/// TestApp
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool res = false;

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    SmsServices.receiveSms().listen(print);

    final bool response = await SmsServices.sendSms(
      destinationAddress :'0652229850',
      message: 'testing message o/'
    );
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted || response == null) {
      return;
    }

    setState(() {
      res = response;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Text(res ? 'Sended' : 'Failed')
      ),
    );
  }
}
