import 'dart:convert';
/// Return a map as a prettified json string
String prettifyJSON(Map<String, dynamic> map) {
  final JsonEncoder encoder = const JsonEncoder.withIndent('  ');
  return encoder.convert(map);
}